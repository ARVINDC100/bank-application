<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="Register.css">
<meta charset="UTF-8">
<title>Registration Form</title>
</head>
<body>
<div id="container">
<form action="Register" method="post" align="center">
  <fieldset>
<div class="hea">Registration Details</div>
<input type="text" name="FirstName" placeholder="Firstname" required autofocus><br><br>
<input type="text" name="LastName" placeholder="Lastname" required ><br><br>
<input type="date" name="DOB" placeholder="DOB" required ><br><br>
<input type="radio" name="gender" id ="g1" value="male" >
<label for="g1">Male</label>
<input type="radio" name="gender" id="g2" value="female" >
<label for="g2">Female</label><br><br>
<input type="number" name="aadharno" placeholder="Aadhar Number" required ><br><br>
<input type="number" name="contactno" placeholder="Phone Number" required><br><br>
<input type="text" name="address" placeholder="Full Address" required><br><br>
<input type="text" name="UserName" placeholder="Username" required><br><br>
<input type="password" name="Password" placeholder="Password" required><br><br>
<input type="email" name="email" placeholder="example@gmail.com" required><br><br>
Account Type : <select type="text" name="acctype" id="acctype" required>
  <option value="savings">Savings</option>
  <option value="salary">Salary</option>
</select><br><br>
<input type="number" name="openbalance" placeholder="Add Minimum Amount 1000" required><br><br>
<!-- 
Branch : <select name="branch" id="branch" required>
  <option value="branch1">Branch1</option>
  <option value="branch2">Branch2</option>
  <option value="branch3">Branch3</option>
  <option value="branch4">Branch4</option>
</select><br><br> -->

<input type="submit" value="submit"><br><br>
</form>
 <div class="signup">Already have Account ?
               <a style="color:red;" href=Login.jsp>Login</a>
               </div>
 </fieldset>

</div>
</body>
</html>


