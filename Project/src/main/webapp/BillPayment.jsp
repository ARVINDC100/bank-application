<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="BillPayment.css">
<meta charset="UTF-8">
<title>Bill payment</title>
</head>
<body>
<header id="showcase">
<div class="showcase-content">
 <div class="logo">
<img src="./Resources/Images/logo.png" alt="">
 </div>
  <ul class="main-nav">
 <li><a  href="Home.jsp">Home</a></li>
 <li><a  href="Deposit.jsp">Deposit</a></li>
<li><a href="Withdraw.jsp">Withdraw</a></li>
<li><a href="Transfer.jsp">Transfer</a></li>
 <li><a class="active" href="BillPayment.jsp">Bill Payment</a></li>
 <li><a href="AccountDetails">Account Details</a></li>
 <li><a href="About.jsp">About us</a></li>
  <li><a href="Home.jsp">Logout</a></li>

          </ul>
          </div>
     
</header>
<div id="container">
 <div class="hea">Bill Payment</div>
<form action="BillPayment" align="center">
<fieldset>
 Type of Bill Payment : <select name="bill" id="bill" required>
  <option value="Electricity">Electricity</option>
  <option value="water">water</option>
  <option value="Rent">Rent</option>
  <option value="Internet">Internet</option>
</select><br><br>
<input type="date" name="billdate" placeholder="date be paid"><br><br>
<input type="number" name="amount" placeholder="Amount to be paid"><br><br>
<input type="password" name="passwords" placeholder="Password"><br><br>
<input type="submit" value="submit">
</fieldset>
</form>
</div>
</body>
</html>