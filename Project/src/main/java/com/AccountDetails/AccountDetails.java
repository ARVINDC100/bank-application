package com.AccountDetails;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class AccountDetails
 */
@WebServlet("/AccountDetails")
public class AccountDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
//		String user=request.getParameter("username");
//		String accno=request.getParameter("accountno");
//		long accountno=Long.parseLong(accno);
		HttpSession se =request.getSession();
		String user=(String) se.getAttribute("user");
		int totalamount=0;
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank","root","password");
			PreparedStatement st=con.prepareStatement("Select accountno,username,firstname,minbalance from Userdetails where username=? ");
			st.setString(1, user);
			PreparedStatement st2=con.prepareStatement("Select amount from bill where username=? ");
			st2.setString(1, user);
//			st2.setInt(2,amount);
			ResultSet rs2 = st2.executeQuery();
			while(rs2.next())
			{
				totalamount=totalamount+rs2.getInt(1);
			}
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				out.println("<html>"
						+"<link rel='stylesheet' href='AccountDetails.css'>"
						+ "<body>"
						+"<header id='showcase'>"
						+"<div class='showcase-content'>"
						 
						 +" <ul class='main-nav'>"
						+"<li><a  href='Home.jsp'>Home</a></li>"
						+"<li><a href='Deposit.jsp'>Deposit</a></li>"
						+"<li><a href='Withdraw.jsp'>Withdraw</a></li>"
						+"<li><a href='Transfer.jsp'>Transfer</a></li>"
						+"<li><a href='BillPayment.jsp'>Bill Payment</a></li>"
						+"<li><a class='active' href='AccountDetails'>Account Details</a></li>"
						+" <li><a href='About.jsp'>About us</a></li>"
						+"<li><a href='Home.jsp'>Logout</a></li>"
						       +"  </ul>"
						       +" </div>"
						         +" </header>"
						+"<div id='container'>"
						+"<div class='hea'>Your Account Details</div>"
						+ "<center>");
				out.println("<h2>ACCOUNT NUMBER : "+rs.getLong(1)+"</h2>");
				out.println("<h2>USERNAME : "+rs.getString(2)+"</h2>");
				out.println("<h2>FIRSTNAME : "+rs.getString(3)+"</h2>");
				
				out.println("<h2>BALANCE : "+rs.getLong(4)+"</h2>");
				out.println("<h2>Bill Paid Till Date : "+totalamount+"</h2><br><br>");
				out.println("</center>");
				out.println("</body>");
				out.println("</html>");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
