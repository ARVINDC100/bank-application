package com.Transfer;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Transfer
 */
@WebServlet("/Transfer")
public class Transfer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
				
			String pass = request.getParameter("passwords");
			Long accountno = Long.parseLong(request.getParameter("accountno"));
			int amount=Integer.parseInt(request.getParameter("transfer"));
			HttpSession se =request.getSession();
			
			String user=(String) se.getAttribute("user");
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank","root","password");
				Statement stmt =con.createStatement() ;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con2=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank","root","password");
				Statement stmt=con2.createStatement();
				PreparedStatement st=con2.prepareStatement("Select minbalance,passwords,accountno from Userdetails  where username=? and passwords=?");
				PreparedStatement st2=con2.prepareStatement("Select minbalance from Userdetails  where accountno=? ");
				st.setString(1, user);
				st.setString(2, pass);
//				st.setLong(2, amount);
				
				st2.setLong(1,accountno);
				ResultSet rs2=null;
				rs2=st2.executeQuery();
			
				
				ResultSet rs = null;
					rs = st.executeQuery();
				
					if (rs.next()) {
						 if(amount<(rs.getDouble(1)))
						 {
							 if((rs.getDouble(1)-amount)>=1000)
							 {
								 if(rs2.next())
								 {
									 if(rs.getLong(3)!=accountno)
									 {
										 if(amount>0)
										 {
											 String query = "update Userdetails set minbalance = " + (rs.getDouble(1)-amount)+" where username = '"+user+"' and passwords = '"+ pass+"'";
					    						stmt.executeUpdate(query);
												 String query2 = "update Userdetails set minbalance = " + (rs2.getDouble(1)+amount)+" where accountno = '"+accountno+"'";
												 stmt.executeUpdate(query2);
					    					   	   out.println("<script type=text/javascript>");
					    						   out.println("alert('Amount Transfered');");
					    						   out.println("location='Dashboard.jsp';");
					    						   out.println("</script>");
										 }
										 else
										 {
											 out.println("<script type=text/javascript>");
				    						   out.println("alert('amount should be positive and greater than 0');");
				    						   out.println("location='Transfer.jsp';");
				    						   out.println("</script>");
										 }
										
									 }
									 else
									 {
										 
				    						   out.println("<script type=text/javascript>");
				    						   out.println("alert('Cannot transfer to same account');");
				    						   out.println("location='Transfer.jsp';");
				    						   out.println("</script>");
									 }
									 
								 }
								 else
								 {
									 out.println("<script type=text/javascript>");
			  						   out.println("alert('Account doesnot exist');");
			  						   out.println("location='Transfer.jsp';");
			  						   out.println("</script>");

								 }
							 }
							 else
							 {
								 out.println("<script type=text/javascript>");
	      						   out.println("alert('Min balance 1000 should be maintained');");
	      						   out.println("location='Transfer.jsp';");
	      						   out.println("</script>");
							 }
						 }
						 else
						 {
						   out.println("<script type=text/javascript>");
  						   out.println("alert('Insufficient balance');");
  						   out.println("location='Transfer.jsp';");
  						   out.println("</script>");
						 }
						
						  
					}
					else {
						   out.println("<script type=text/javascript>");
						   out.println("alert('invalid credentials');");
						   out.println("location='Transfer.jsp';");
						   out.println("</script>");
					}
					
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
