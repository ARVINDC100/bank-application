package com.Register;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String user=request.getParameter("UserName");
		String password=request.getParameter("Password");
		String firstname=request.getParameter("FirstName");
		String lastname=request.getParameter("LastName");
		String dob=request.getParameter("DOB");
		Random rand = new Random(); 
		long accountno = rand.nextInt(9000000) + 1000000;
		String Email=request.getParameter("email");
		String gender=request.getParameter("gender");
		String adhar=request.getParameter("aadharno");
		long aadharno=Long.parseLong(adhar);
		String contact=request.getParameter("contactno");
		int contactno=Integer.parseInt(contact);
		String address=request.getParameter("address");
	//	String branch=request.getParameter("branch");
		String minbal=request.getParameter("openbalance");
		long openbalance=Integer.parseInt(minbal);
		String accounttype=request.getParameter("acctype");
		out.println(user+""+password);
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank", "root", "password");
			PreparedStatement st=con.prepareStatement("insert into Userdetails values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
			st.setString(1, user);
			st.setString(2, password);
			st.setString(3, firstname);
			st.setString(4, lastname);
			st.setString(5, dob);
			st.setLong(6, accountno);
			st.setString(7, Email);
			st.setString(8, gender);
			st.setLong(9, contactno);
			st.setString(10, address);
		//	st.setString(11, branch);
			st.setLong(11, aadharno);
			st.setLong(12, openbalance);
			st.setString(13, accounttype);
			
			
			PreparedStatement st2=con.prepareStatement("Select username from Userdetails where username=?");
			st2.setString(1, user);
			ResultSet rs=st2.executeQuery();
			if(rs.next())
				{
				
				out.println("<script type=text/javascript>");
				   out.println("alert('User Name Already Exists');");
				   out.println("location='Register.jsp';");
				   out.println("</script>");
				}
			if(openbalance>1000)
			{
				st.execute();
				response.sendRedirect("Dashboard.jsp");
			}
			else
			{
				out.println("<script type=text/javascript>");
				   out.println("alert('Minimum Balance Shiuld be 1000');");
				   out.println("location='Register.jsp';");
				   out.println("</script>");

			}
			
			
			}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
