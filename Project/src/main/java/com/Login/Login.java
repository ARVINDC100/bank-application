package com.Login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String user=request.getParameter("username");
		String password=request.getParameter("password");
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank","root","password");
			PreparedStatement st=con.prepareStatement("Select username,passwords from Userdetails where username=? and passwords=?");
			st.setString(1,user);
			st.setString(2, password);
			ResultSet rs=st.executeQuery();
			if(rs.next())
			{
				HttpSession se=request.getSession();
				se.setAttribute("user",user );
				
					out.println("alert('login success')");
					response.sendRedirect("Dashboard.jsp");
			}
			else 
			{
				
				out.println("<script type=text/javascript>");
				   out.println("alert('login failed');");
				   out.println("location='Login.jsp';");
				   out.println("</script>");
			}
			
		} 
		
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
