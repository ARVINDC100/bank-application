package com.BillPayment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/BillPayment")
public class BillPayment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String pass = request.getParameter("passwords");
		String bill = request.getParameter("bill");
		String bill2="Electricity";
		Date billdate1=null;
		try {
			billdate1 = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("billdate"));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String billdate = request.getParameter("billdate");
		int amount=Integer.parseInt(request.getParameter("amount"));
		String m="0000-00-00";
		Date m1=null;
		try {
			m1 = new SimpleDateFormat("yyyy-MM-dd").parse(m);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		System.out.println(billdate1.getMonth());
		
		HttpSession se =request.getSession();
		
		String user=(String) se.getAttribute("user");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank","root","password");
			Statement stmt =con.createStatement() ;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con2=DriverManager.getConnection("jdbc:mysql://localhost:3306/Bank","root","password");
			Statement stmt=con2.createStatement();
			PreparedStatement st=con2.prepareStatement("Select minbalance,passwords from Userdetails  where username=? and passwords=?");
			
			PreparedStatement st3=con2.prepareStatement("insert into bill values(?,?,?,?)");
			
			
			st.setString(1, user);
			st.setString(2, pass);
//			st.setLong(2, amount);
			st3.setString(1, user);
			st3.setString(2, billdate);
			st3.setInt(3, amount);
			st3.setString(4, bill);
			
			
			PreparedStatement st4=con2.prepareStatement("select billdate,billtype from bill where username=?");
			st4.setString(1, user);
			ResultSet rs4=null;
			rs4=st4.executeQuery();
			if(rs4.next())
			{
				m1=rs4.getDate(1);
				bill2=rs4.getString(2);
			}
			
//			System.out.println(m1.getMonth());
//			System.out.println(billdate1.getMonth()==m1.getMonth());
//			System.out.println(bill);
//			System.out.println(bill2);
//			System.out.println(bill.equals(bill2));
//			System.out.println(m1.equals(m));
//			
			ResultSet rs = null;
				rs = st.executeQuery();
			if(billdate1.getMonth()==m1.getMonth())
			{
				  out.println("<script type=text/javascript>");
				   out.println("alert('Your Bill For This Month is Paid');");
				   out.println("location='BillPayment.jsp';");
				   out.println("</script>");
				  }
			else
			{
				
			
				if (rs.next() && amount>0) {
					 if(amount<(rs.getDouble(1)))
					 {
						 if((rs.getDouble(1)-amount)>=1000)
						 {
							 
							 st3.execute();
							 String query = "update Userdetails set minbalance = " + (rs.getDouble(1)-amount)+" where username = '"+user+"' and passwords = '"+ pass+"'";
	    						stmt.executeUpdate(query);

	    					   	   out.println("<script type=text/javascript>");
	    						   out.println("alert('Bill Paid');");
	    						   out.println("location='Dashboard.jsp';");
	    						   out.println("</script>");
						 }
						 else
						 {
							 out.println("<script type=text/javascript>");
      						   out.println("alert('Not Allowed Since Min balance 1000 should be maintained');");
      						   out.println("location='BillPayment.jsp';");
      						   out.println("</script>");
						 }
					 }
					 else
					 {
					   out.println("<script type=text/javascript>");
						   out.println("alert('Insufficient balance');");
						   out.println("location='BillPayment.jsp';");
						   out.println("</script>");
					 }
					
					  
				}
				else {
					   out.println("<script type=text/javascript>");
					   out.println("alert('invalid credentials');");
					   out.println("location='BillPayment.jsp';");
					   out.println("</script>");
				}
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}


